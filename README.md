# Proiect

This is the 3rd Project for our class presenting Lifecycle Hooks & Directives.


# Learning Git Game: 

https://learngitbranching.js.org/
- create a new branch: 
    `git branch <branch-name>`

- move to the new branch 
    `git checkout <branch-name>`

- create a new branch and move it automatically 
   `git checkout -b <branch-name>`

- get the latest changes from the remote branch you are currently checkout out as
    `git pull`

- push all your local commits to an existing remote branch
   `git push`

- push all your local commits to a new remote branch
    `git push -u origin <local-branch-name>`

- Merge request has conflicts. Steps to solve them:  
    1. git checkout master
    2. git pull
    3. git checkout <branch-name> (branchul pt care ai facut merge request)
    4. git merge master 
    5. rezolva conflictele
    6. git add .
    7. git commit -m "merged master" 
    8. git push 